
## Script for creating GRanges object from large Bowtie files
## Manu Setty
## 04/04/2011

## Maps for chromsome and strands
chrs.map <- 1:25
names (chrs.map) <- sprintf ('chr%d', chrs.map)
names (chrs.map)[23] <- 'chrX'
names (chrs.map)[24] <- 'chrY'
names (chrs.map)[25] <- 'chrM'

strands.map <- 1:2
names (strands.map) <- c('+', '-')


read.bowtie.batch <- function (alignment.file, shift=0, tag.length=NULL,
                               convert.to.list=TRUE, include.ids=FALSE, include.mismatches=FALSE) {
  ## Check if Rdata file has been saved
  rdata.file <- sprintf ('%s.Rdata', alignment.file)
  if (file.exists (rdata.file)) {
    load (rdata.file)
    .tags <<- .tags
    invisible (.tags)
  }

  
  ## Function to read in parallel. Input is the file seek position
  read.thread <- function (file.ptr) {

    counter <- 0
    storage.mode (file.ptr) <- 'double'
    storage.mode (counter) <- 'integer'
    ## Read the bowtie tags
    res <- .Call ("read_bowtie", alignment.file, file.ptr, counter, MAX.COUNT)

    if (counter == 0)
      return (NULL)

    ## Set up Ranges objects
    inds <- 1:counter
    return (list (seqnames=Rle (chrs.map[res$chrs[inds]]),
                  ranges=IRanges (start=res$positions[inds]+1+shift, end=res$positions[inds]+tag.length+shift),
                  strands=Rle (strands.map[res$strands[inds]]),
                  ids=BStringSet (res$ids[inds])
                  ))
  }  

  ## Reads MAX.COUNT tags in a batch
  MAX.COUNT <- 5000000
  storage.mode (MAX.COUNT) <- "integer"
  STRLEN <- 100

  ## Determine tag length is not specified as a parameter
  if (is.null (tag.length))
    tag.length <- nchar (scan (alignment.file, nlines=1, what='character', sep='\t')[5])
  
  ## Determine file seek positions
  file.indices <- rep (0,100)
  index.count <- 0
  res <- .C("scan_file", alignment.file, as.integer (MAX.COUNT),
             index.count=as.integer (index.count), file.indices=as.double (file.indices))
  file.indices <- res$file.indices[1:res$index.count]
  
  ## Read file
  registerDoMC (.no.threads)
  all.data <- foreach (index=file.indices) %dopar% read.thread (index)
  all.data <- all.data[!sapply (all.data, is.null)]
  
  ## Combine the different threads
  combined.data <- list ()
  for (l in labels(all.data[[1]]) ) {
    combined.data[[l]] <- all.data[[1]][[l]]
    for (i in 2:length (all.data)) {
      combined.data[[l]] <- append (combined.data[[l]], all.data[[i]][[l]])
      all.data[[i]][[l]] <- NULL
    }
  }
  all.data <- NULL
  gc ()

  ## Reset chromosomes and strands
  combined.data$seqnames <- Rle (names (chrs.map)[as.numeric (combined.data$seqnames)])
  combined.data$strands <- Rle (names (strands.map)[as.numeric (combined.data$strands)])
  
  ## Create GRanges object/ GRanges list object
  if (!convert.to.list) {
    .tags <- GRanges (combined.data$seqnames, combined.data$ranges, combined.data$strands, ids=combined.data$ids)
  }
  else {
    chrs <- as.factor (combined.data$seqnames)
    inds <- tapply (1:length (chrs), chrs, list)
    .tags <- list ()
    for (i in names (inds)) 
      .tags[[i]] <- GRanges (combined.data$seqnames[inds[[i]]], combined.data$ranges[inds[[i]]],
                             combined.data$strands[inds[[i]]], ids=combined.data$ids[inds[[i]]])
  }

  .tags <<- .tags
  save (.tags, file=rdata.file)
  invisible (.tags)
}

