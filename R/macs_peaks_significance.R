
## Script to determine significance of peaks
## Based on MACS14

# Revised             Comments
# 02/12/2013          Adjust length of coverage vector to be greater than end point in ranges


#' @export
determine.peaks.significance <- function (bam.file, peaks, tag.size=50,
                                          outer.window=10000, gsize=2.7e9, no.threads=3) {

  show ('Loading peaks data....')
  ## Load peaks
  if (class (peaks) == 'character') {
    data <- read.table (peaks, header=TRUE)
    peaks <- GRanges (data[,1], IRanges (data[,2], data[,3]),
                         summit.pos=data[,5] - data[,2] + 1)
  } 
  peaks.list <- granges.to.list (peaks)
    
  ## Determine height, summit, tag counts in all peaks
  chr.func <- function (chr) {
    show (chr)
    params <- ScanBamParam(what = c("strand", "pos", "rname", "qwidth"),
                           which = GRanges (chr, IRanges (1, 536870912)),
                           flag = scanBamFlag(isUnmappedQuery = FALSE))
    data <- scanBam(bam.file, param = params)[[1]]
    tags <- ranges (resize (GRanges (data$rname, IRanges (data$pos, data$qwidth + data$pos - 1), data$strand),
                            tag.size, fix='start'))
    cov <- coverage (tags)
    if (!is.na (seqlengths (peaks)[chr]) & length (cov) < seqlengths(peaks)[chr])
      cov <- append (cov, rep (0, seqlengths(peaks)[chr] - length (cov)))
    if (max (end (peaks.list[[chr]])) > length (cov))
      cov <- append (cov, rep (0, max (end (peaks.list[[chr]])) - length (cov)))
    
    ## Scores, heights and summit positions
    dd <- .get.peak.coverage (peaks.list[[chr]], cov)
    values (peaks.list[[chr]])$height <- dd[,'height']
    values (peaks.list[[chr]])$summit.pos <- dd[,'summit.pos']
    values (peaks.list[[chr]])$score <- countOverlaps (ranges (peaks.list[[chr]]), tags, minoverlap=floor (tag.size/2))

    ## Count overlaps in extended region
    extended.peaks <- resize (peaks.list[[chr]], outer.window, fix='center')
    values (peaks.list[[chr]])$score.outer <- countOverlaps (ranges (extended.peaks), tags, minoverlap=floor (tag.size/2))
    
    return (peaks.list[[chr]])
  }

  ## Determine counts
  show ('Determining counts...')
  registerDoMC (no.threads)
  peaks <- foreach (chr = names (peaks.list), .combine=c) %dopar% chr.func (chr)

  ## Local lambda
  show ('Lambda determination...')
  total.tag.count <- bam.tag.count (bam.file)
  effective.widths <- pmax (tag.size * 2, width (peaks))
  lambda.local <- effective.widths * total.tag.count / gsize
  ## Background lambda
  lambda.outer <- values (peaks)$score.outer/outer.window * effective.widths
  lambda.selected <- pmax (lambda.local, lambda.outer)

  ## Calculate pvalues
  show ('p-value estimation...')
  values (peaks)$pvalues <- round (-10 * log10 (apply (cbind (lambda.selected, values (peaks)$score * effective.widths / width (peaks)), 1,
                                                          function (x) { ppois (x[2], x[1], lower.tail=FALSE) })), 2)

  ## Remove infinities
  values (peaks)$pvalues[is.infinite (values (peaks)$pvalues)] <- 3100
  
  return (peaks)
}


