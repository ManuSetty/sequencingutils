#include <stdio.h>
#include <string.h>
#include <R.h>
#include <Rinternals.h>
#define MAX 1000

SEXP read_bowtie (SEXP align_file, SEXP file_ptr, SEXP counter, SEXP max_count) {

  const char *calign_file = CHAR(STRING_ELT(align_file, 0));
  int *cmax_count = INTEGER (max_count);
  int *ccounter = INTEGER (counter);

  // Create the containers
  SEXP seq_ids, strands, chrs, positions;
  // Allocate memory to containers
  PROTECT (seq_ids = allocVector (STRSXP, *cmax_count));
  PROTECT (strands = allocVector (STRSXP, *cmax_count));
  PROTECT (chrs = allocVector (STRSXP, *cmax_count));
  PROTECT (positions = allocVector (INTSXP, *cmax_count));
  int *cpos = INTEGER (positions);

  FILE *fp = fopen (calign_file, "r");
  double *cfile_ptr = REAL (file_ptr);
  fseek (fp, (long int)*cfile_ptr, SEEK_SET);
  char container[MAX];
  char *tokens;

  *ccounter = 0;
  while (*ccounter < *cmax_count) {
    if (fgets (container, MAX, fp) == NULL)
      break;

    // Tokenize the line and read in each entry
    tokens = strtok (container, "\t");
    SET_STRING_ELT (seq_ids, *ccounter, mkChar (tokens));
    tokens = strtok (NULL, "\t");
    SET_STRING_ELT (strands, *ccounter, mkChar (tokens));
    tokens = strtok (NULL, "\t");
    SET_STRING_ELT (chrs, *ccounter, mkChar (tokens));
    tokens = strtok (NULL, "\t");
    cpos[(*ccounter)++] = atoi (tokens);
  }

  // Create a list with everything
  SEXP ret_list;
  PROTECT (ret_list = allocVector (VECSXP, 4));
  SET_VECTOR_ELT (ret_list, 0, seq_ids);
  SET_VECTOR_ELT (ret_list, 1, strands);
  SET_VECTOR_ELT (ret_list, 2, chrs);
  SET_VECTOR_ELT (ret_list, 3, positions);

  SEXP attribs;
  PROTECT (attribs = allocVector (VECSXP, 4));
  SET_VECTOR_ELT (attribs, 0, mkChar ("ids"));
  SET_VECTOR_ELT (attribs, 1, mkChar ("strands"));
  SET_VECTOR_ELT (attribs, 2, mkChar ("chrs"));
  SET_VECTOR_ELT (attribs, 3, mkChar ("positions"));
  setAttrib (ret_list, R_NamesSymbol, attribs);

  UNPROTECT (6);
  return (ret_list);
}


// Read through the file and find the file pointer indexes
void scan_file (char **align_file, int *count_max, 
		int *index_count, double *indices) {

    FILE *fp = fopen (*align_file, "r");
    int counter =0;
    char str[MAX];

    // Read through the file and determine indices
    *index_count=0;
    indices[(*index_count)++] = 0;
    while (fgets (str, MAX, fp)!=NULL) { 
      counter++;
      if (counter == *count_max) {
	counter = 0;
	indices[(*index_count)++] = ftell (fp);
      }
    }
}
