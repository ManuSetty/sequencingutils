#include<stdio.h>
#include<string.h>

void poisson_peaks (int *tags, int *tag_ends, int *total_tags,
		    char **chr, char **out_file,
		    int *window_size, int *min_tags
		    ) {

  FILE *fp = fopen (*out_file, "w");
  int start_ind=0, end_ind=0, num_tags=1;
  int curr_start=0, curr_end=0;
  int tag_ind=0, j;

  fprintf (fp, "Chr\tStart\tEnd\tCount\n");
  for (tag_ind=1; tag_ind<*total_tags; tag_ind++) {
    // Check if the window size is exceeded
    if (tags[tag_ind] - tags[start_ind] > *window_size) {
      // Number of tags
      if (num_tags >= *min_tags) {
	if (tag_ends[curr_end] >= tags[start_ind])
	  curr_end = end_ind;
	else {
	  // Write determine peak to file
	  fprintf (fp, "%s\t%d\t%d\t%d\n", *chr, tags[start_ind], tag_ends[end_ind], end_ind-start_ind+1);
	  curr_start = start_ind;
	  curr_end = end_ind;
	}
      }
      for (; start_ind<=tag_ind && tags[tag_ind]-tags[start_ind]>*window_size;start_ind++)
	num_tags--;
    }
    end_ind = tag_ind;
    num_tags++;
  }
  
  // Last peak if applicable
  if (num_tags >= *min_tags)
    fprintf (fp, "%s\t%d\t%d\t%d\n", *chr, tags[start_ind], tag_ends[end_ind], end_ind-start_ind+1);
  
  fclose (fp);

}

