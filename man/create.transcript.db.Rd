\name{create.transcript.db}
\alias{create.transcript.db}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
makeTranscriptDbFromUCSC wrapper
}
\description{
Uses \code{makeTranscriptDbFromUCSC} function to create transcript db and save it
}
\usage{
create.transcript.db(out.dir, genome = "hg18", tablename = "refGene", ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{out.dir}{
Output Directory 
}
  \item{genome}{
Default hg18
}
  \item{tablename}{
Default refGene
}
  \item{\dots}{
Additional arguments to makeTranscriptDbFromUCSC
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Saves a sqlite file and returns the created database
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{makeTranscriptDbFromUCSC}}
}
