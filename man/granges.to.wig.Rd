\name{granges.to.wig}
\alias{granges.to.wig}

\title{
Granges List to Wig files
}
\description{
Determines coverage and converts to wig files
}
\usage{
granges.to.wig(tags.list, out.dir = ".", out.file = "NA", collapse = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tags.list}{
GRangesList object
}
  \item{out.dir}{
Output directory
}
  \item{out.file}{
Name of the output file. Ignored if \code{collapse} is \code{FALSE}
}
  \item{collapse}{
Logical indicating whether data from all chormosomes should be written to same file.
}
}
\details{

}
\value{
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{coverage.parallel}}
\code{\link{coverage.to.wig}}
}
