\name{read.bowtie.batch}
\alias{read.bowtie.batch}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Read large bowtie files and create GRanges object
}
\description{
Scans through the bowtie file to determine seek indices and loads the
file in parallel using \code{.no.threads} processes. Should be used when
reading in >20M tags. Use \code{read.aligned.file} for smaller files.
}
\usage{
read.bowtie.batch(alignment.file, shift = 0, tag.length = NULL, convert.to.list = TRUE, include.ids = FALSE, include.mismatches = FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{alignment.file}{
Bowtie alignment file
}
  \item{shift}{
Tag shift length
}
  \item{tag.length}{
Tag size. If the parameter is not specified, the length is determined
from bowtie output
}
  \item{convert.to.list}{
Should the GRanges object be converted to a list
}
  \item{include.ids, include.mismatches}{
Not implemented. Ids are always included now.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
\code{.tags} object will be saved to Rdata file and returned.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

